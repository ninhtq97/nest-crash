import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateItemDto } from './dto/create-item.dto';
import { ItemsService } from './items.service';
import { Item } from './schemas/item.schema';

@Controller('items')
export class ItemsController {
  constructor(private readonly itemsService: ItemsService) {}

  @Get()
  getAll(): Promise<Item[]> {
    return this.itemsService.getAll();
  }

  @Get(':id')
  getOne(@Param('id') id): Promise<Item> {
    return this.itemsService.getOne(id);
  }

  @Post()
  create(@Body() itemDto: CreateItemDto): Promise<Item> {
    return this.itemsService.create(itemDto);
  }

  @Put(':id')
  update(@Body() itemDto: CreateItemDto, @Param('id') id): Promise<string> {
    return this.itemsService.update(id, itemDto);
  }

  @Delete(':id')
  delete(@Param('id') id): Promise<string> {
    return this.itemsService.delete(id);
  }
}
