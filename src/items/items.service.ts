import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateItemDto } from './dto/create-item.dto';
import { Item } from './schemas/item.schema';

console.log(Item);

@Injectable()
export class ItemsService {
  constructor(@InjectModel(Item.name) private itemModel: Model<Item>) {}

  async getAll(): Promise<Item[]> {
    const items = await this.itemModel.find();

    return items;
  }

  async getOne(id: string): Promise<Item> {
    const item = await this.itemModel.findById(id);
    return item;
  }

  async create(item: CreateItemDto): Promise<Item> {
    const newItem = await this.itemModel.create(item);
    return newItem;
  }

  async update(id: string, item: CreateItemDto): Promise<string> {
    await this.itemModel.findByIdAndUpdate(id, item);
    return 'Update success';
  }

  async delete(id: string): Promise<string> {
    await this.itemModel.findByIdAndRemove(id);
    return 'Delete success';
  }
}
